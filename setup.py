from setuptools import setup

setup(
   name='SecretsManager',
   version='1.0',
   description='AWS Secrets Manager Client',
   author='Rohan Sundar',
   author_email='rohan.doodleblue@samunnati.com',
   packages=['secretsmanager'],
   install_requires=['boto3'], #external packages as dependencies
)